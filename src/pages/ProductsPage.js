import ProductsList from "../components/ProductsList/ProductsList";

const ProductsPage = () => {
  return (
    <>
      <h1>products page</h1>
      <ProductsList />
    </>
  );
};
export default ProductsPage;
