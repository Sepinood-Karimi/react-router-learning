import { Link, useNavigate } from "react-router-dom";

const HomePage = () => {
  const navigate = useNavigate();
  const submitHandler = () => {
    navigate("/products");
  };
  return (
    <>
      <h1>My Home Page</h1>
      <p>
        Go to <Link to="/products">the products page</Link>
      </p>
      <div>
        <button onClick={submitHandler}>Submit</button>
      </div>
    </>
  );
};
export default HomePage;
