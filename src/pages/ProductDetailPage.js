import { Link, useParams } from "react-router-dom";

const ProductDetailPage = () => {
  const params = useParams();

  return (
    <>
      <h3> product detail page </h3>
      <p> id of product : {params.productId} </p>
      <button>
        <Link to=".." relative="path">
          Back
        </Link>
      </button>
    </>
  );
};
export default ProductDetailPage;
