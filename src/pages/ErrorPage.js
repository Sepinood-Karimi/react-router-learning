import Navbar from "../components/Navbar/Navbar";

const ErrorPage = () => {
  return (
    <main>
      <Navbar />
      <h1>Error Page</h1>
    </main>
  );
};
export default ErrorPage;
