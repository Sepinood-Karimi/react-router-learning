import { NavLink } from "react-router-dom";
import classnames from "classnames";
import classes from "./NavBar.module.css";

const Navbar = () => {
  return (
    <nav className={classnames(classes.navbar)}>
      <ul>
        <li>
          <NavLink
            to="/"
            className={({ isActive }) =>
              isActive ? classes.active : undefined
            }
            end
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/products"
            className={({ isActive }) =>
              isActive ? classes.active : undefined
            }
            end
          >
            Products
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};
export default Navbar;
