import { Link } from "react-router-dom";
import classes from "./ProductsList.module.css";
import classnames from "classnames";

const ProductsList = () => {
  const dummyProducts = [
    { id: "p1", title: "Product 1" },
    { id: "p2", title: "Product 2" },
    { id: "p3", title: "Product 3" },
  ];
  return (
    <ul className={classnames(classes.list)}>
      {dummyProducts.map((product) => (
        <Link to={`/products/${product.id}`}>{product.title}</Link>
      ))}
    </ul>
  );
};
export default ProductsList;
